<?php
    $id = $_POST['order'];
    $email = $_POST['email'];
    $alamat = $_POST['alamat'];
    $member = $_POST['member'];
    $pembayaran = $_POST['bayar'];
    $nama = $_POST['namap'];

    $harga=0;

    if(isset($_POST['coklat'])){
        $harga=$harga +28000;
    }
    if(isset($_POST['matcha'])){
        $harga=$harga +18000;
    }
    if(isset($_POST['mojicha'])){
        $harga=$harga +15000;
    }
    if(isset($_POST['latte'])){
        $harga=$harga +30000;
    }
    if(isset($_POST['taro'])){
        $harga=$harga +21000;
    }
    if($member=="iyes"){
        $diskon=$harga*0.1;
        $harga=$harga-$diskon;
    }  
    
?>


<html>
    <head>
        
        <title>Nota</title>

        <!--boostrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" 
        integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" 
        crossorigin="anonymous">

        <style>
            .form{
                margin-right: 25%;
                margin-left: 25%;
                margin-top:7%;
            }

            .judul{
                opacity:0.5;
            }
            p{
                margin-top:20px;
            }
            .isi{
                border-collapse: collapse;
            }
            td{
                padding-top: 17px;
                padding-bottom: 17px;
                border-bottom: 1px solid black;
                width:200px;
            }
            h1{
                margin-bottom:20px;
            }

        </style>
    </head>

    <body>
        <div class="form" align="center">
            <h1 class="judul">Transaksi <br> Pemesanan</h1> 
            <p>Terimakasih telah berbelanja dengan Kopi Susu Duarrr!</p>

            <br>

            
            <h1>Rp. <?= $harga?>.00,-</h1>

            <table class="isi">
                <tr>
                    <td><b>ID</b></td>
                    <td><?= $id ?></td>
                </tr>
                <tr>
                    <td><b>Nama</b></td>
                    <td><?= $nama ?></td>
                </tr>
                <tr>
                    <td><b>Email</b></td>
                    <td><?= $email ?></td>
                </tr>
                <tr>
                    <td><b>Alamat</b></td>
                    <td><?= $alamat?></td>
                </tr>
                <tr>
                    <td><b>Member</b></th>
                    <td> <?= $member ?></td>
                </tr>
                <tr>
                    <td><b>Pembayaran</b></td>
                    <td> <?= $pembayaran ?></td>
                </tr>

            </table>
        </div>
    </body>
</html>  