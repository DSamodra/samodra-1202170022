<?php

        $nama = $_GET['nama'];
        $tanggal = $_GET['t'];
        $telpon = $_GET['telpon'];
        $driver = $_GET['driver'];
        $ktg = $_GET['kantong'];  
      
?>

<html>
    <head>

        <title>menu</title>

        <!--boostrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" 
        integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" 
        crossorigin="anonymous">

        <!-- Jquery -->

        <script  
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
        </script>

        <style>
            .data{
                text-align:center;
            }
            .kanan{
                margin-top:5%;
                padding-right:15%;
                text-align:center;
            }
            .kiri{
                margin-top:5%;
                padding-left:15%;
                text-align:center;
            }
            h1{
                margin-bottom:40px;
                font-size:50px;
                opacity:0.5;
            }
            p{
                margin-top:15px;
                margin-bottom:15px;
            }
            label{
                font-size: 25px;
            }
            .menu{
                    border-collapse: collapse; 
            }
            td{
                border-top: 1px solid black;
                border-bottom: 1px solid black;
                padding-top: 15px;
                padding-bottom: 10px;
                padding:15px;
            }
            .submit{
                border-radius:7px;
                height: 50px;
                width: 400px;
                background-color: purple;
                color: white;
            }
            .balik{
                background-color:purple;
                color: white;
                border-radius:7px;
                font-size:16px;
                height: 50px;
                width: 150px;
                margin-top:15px;
            }
            label{
                font-size:20px;
                opacity:0.8;
            }
        </style>

    </head>

    <body>
        <div>
            <div class="kiri" style="float: left">  <!-- form kiri-->

                <form method="GET" action="form.html">
                    <h1>~Data Driver Ojol~</h1>

                    <label><b>Nama</b></label>
                    <p><?= $nama ?></p>

                    <label><b>Nomor Telepon</b></label>
                    <p><?= $telpon ?></p>

                    <label><b>Tanggal</b></label>
                    <p><?= $tanggal ?></p>

                    <label><b>Asal Driver</b></label>
                    <p><?= $driver ?></p>

                    <label><b>Bawa Kantong</b></label>
                    <p><?= $ktg ?></p>

                    <button type="submit" class="balik" > &lt&lt Kembali</button>
                </form>
            </div>  

            <div class="kanan" style = "float: right"> <!--form kanan-->
            <h1>~Menu~</h1>
                <p>Pilih Menu</p>

                <form method="POST" action="nota.php">

                    <table class="menu" align="center">
                        <tr>
                            <td><input type="checkbox" name="coklat" id="coklat">  Es Coklat Susu</td>
                            <td>Rp. 28.000,-</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="matcha" id="matcha">  Es Susu Matcha</td>
                            <td>Rp. 18.000,-</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="mojicha" id="mocjicha">  Es Susu Mojicha</td>
                            <td>Rp. 15.000,-</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="latte" id="latte">  Es Matcha Latte</td>
                            <td>Rp. 30.000,-</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="taro" id="taro">  Es Taro Susu</td>
                            <td>Rp. 21.000,-</td>
                        </tr>
                        <tr>
                            <td>Nomor Order</td>
                            <td><input type="number" name="order" id="order" required></td>
                        </tr>
                        <tr>
                            <td>Nama Pemesan</td>
                            <td><input type="text" name="namap" id="namap" required></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="email" name="email" id="email" required></td>
                        </tr>
                        <tr>
                            <td>Alamat Order</td>
                            <td><input type="text" name="alamat" id="alamat" required></td>
                        </tr>
                        <tr>
                            <td>Member</td>
                            <td>
                                <input type="radio" name="member" value="iyes" required>Ya
                                <input type="radio" name="member" value="noes" required>Tidak
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Pembayaran</td>
                            <td>
                            <select name="bayar" required>
                                <option value="" selected disables hidden>--Pilih Metode Pembayaran--</option>
                                <option value="cash">Cash</option>
                                <option value="emoney">E-Money (OVO/GoPay)</option>
                                <option value="credit">Credit Card</option>
                                <option value="lain">Lainnya</option>
                            </select>
                            </td>
                        </tr>
                    </table>

                    <br>
                    <input type="submit" class ="submit" value="Cetak Struk" onclick="">
                    </form>
            </div>
        </div>
    </body>


</html>