@extends('layouts.app')

@section('content')
    @foreach ($data as $r)
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="img">
                            <img class="logo" src="{{ Auth::user()->avatar }}" alt="" width="3%" height="auto">
                        </div>
                        <div class="name">
                        {{ Auth::user()->name }}
                        </div>
                    </div>
                    
                    <!--check connection and image-->
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <img src="{{$r->image}}" alt="logo" width="100%" height="100%" style="margin-left:auto;margin-right:auto" > 
                        
                    </div>

                    <div>
                    <hr>
                    <p class="email"><b>{{ Auth::user()->email }}</b></p>
                    <p class="caption">{{$r->caption}}</p>  
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection
