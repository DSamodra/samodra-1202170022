-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2019 at 12:04 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ead_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_02_111949_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dsamodra77@Gmail.com', '$2y$10$5.F4.x/oPgr4Cepjn4OQ4.ShsDldpD9u.9TfXQUeRlLcR5LHOJeW2', '2019-11-03 04:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `caption`, `image`, `created_at`, `updated_at`) VALUES
(1, 2, 'a5zNS1SpSN', 'k2.jpg', NULL, NULL),
(2, 1, 'Ini Kucing angalo', 'k1.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `title`, `description`, `url`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dimas Samodra Bimaputra', 'dsamodra77@Gmail.com', '$2y$10$l1BCfh9JGkT24on3wdn7aupmqki71nFHtrt59tQhg3vzSzjQdNVNW', NULL, NULL, NULL, 'ur.png', 'CwiLbr8Fwn4PRiYS98ZBfKr3bX0XfbrLRxeeBWTUhQ5N5DAM5K6NlghPa6bw', '2019-11-02 04:36:49', '2019-11-02 04:36:49'),
(2, 'wr7yRewowb', 'W6x5RGYeAa@gmail.com', '$2y$10$/B.aDVIwMcjIaC/hvNOjsOehVKqMhHGzGiL2nQyKROwWw1/CxZO1K', '3YzlsnFy3d', 'eOV8QNGKEW', 'QvUKvVGjHt', 'AlxEiTsrXb', NULL, NULL, NULL),
(3, 'ndfQyEmaMN', 'ErMRyLviiS@gmail.com', '$2y$10$BWYphe1L1ln4VRbFx0DgAO58VdhRQ/wQ.AMr9jw9tpm6tddr2UhCK', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'kXI2iVhuqy', 'LbdISJP4G2@gmail.com', '$2y$10$5hturGn6TRfYPlIvF.ffre1FA4nYUNtLfQuCVMI5QklyS//sTuujK', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'IYjjTeABCt', 'WoGZNBn5xQ@gmail.com', '$2y$10$LfqUxDVM30sRZp3LmnF97ONW.H90G5nmP.m6T8TFX72gadO2yL5RS', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'vaVczfbr5m', 'WzX1mO8mGa@gmail.com', '$2y$10$2EuLP4zj/aB9GL0mynn7iukIxRkeIlPwDjkbZekxHj5d9hsOh5Tgy', 'jKnrR3CQ0b', 'lhsFDxul6n', '5Eb15GECwI', 'pIrZvUEVf7', '2r97jZp68M', NULL, NULL),
(7, 'XHLckDCYcq', '5T7y6bU6mL@gmail.com', '$2y$10$0Clgr/jPIxVW1ZH9la6fuue/hpaMiAzCJPY/qJGQJZmCwMo7AV6G2', 'X5JJ2wmZZD', 'gBo2KMMcVt', 'GZ9c9LRIYD', 'PLU4n81IoI', 'AfZZEyxsPP', NULL, NULL),
(8, 'DSXgee3D9G', 'XitvDW7w9X@gmail.com', '$2y$10$0bld8FlLHcF1ybg4EpUzNuVh9lxcTw/ZKFYP3DV6OomL3BM5B6gLq', 'dXUhL7VBRG', 'spBvCco1eR', 'joIkU43RaM', 'lDqjTXMzU8', 'WrJcw7qusA', NULL, NULL),
(9, '4ECrw57LUh', 'PElqRKdwHB@gmail.com', '$2y$10$zMQv70nnVNqXYrdG5nfexeVRLlmMK0rorgvmEYpZZsaS/Ktx78B4C', 'TdVFLhLxgi', 'yyzEfcW8Xp', '0juc0VVyfW', '7YKLcXm0oc', 'Dz8LhzRxMT', NULL, NULL),
(10, '7upxhQNlvt', 'CP4AAI7OHX@gmail.com', '$2y$10$4MGrbuuZJ1dzNYgabU07JeGeBfvDRuI76krE1QVSMJBp79MxCblDi', 'u1EtK4ZnA4', 'UnrpwtW86W', 'S9h5dzuI9x', '0GS49cGrTB', 'zl630OEags', NULL, NULL),
(11, 'mr0H5pBaOg', 'olrKfMNReE@gmail.com', '$2y$10$UbkuinPkTKGEieHRxD1hCuivfEgyFODZFQeF3ppjURqQTDRAQnPqu', '74IEbzqJy4', 'tyJeJZsHrw', 'YEXv8xvp9Y', 'MueEK3uIZO', 'ySlBkntcwt', NULL, NULL),
(12, 'D4XXVEPGWM', '7A4ksw5IhI@gmail.com', '$2y$10$5FY6tvptnQEDxLj9lp/1jeqrp5RqmQn5gS5IVO1.vO4efP/2pfTtW', 'pGrWGWfp3B', 'S0t33iiFxn', 'szr1ZzDWK9', 'LL5qi7CYWd', 'FbqaMhJlD7', NULL, NULL),
(13, 'rBzpVbIp4B', 'LriHAnaqSP@gmail.com', '$2y$10$D8MD/jt0ySsR32O/b9qpLOaTVUtDlvrQXMcajRqh9ewZFjs5ZHNB6', '4N8PJbVWS7', 'XoWBOOsDOc', '8WK8ZkIkcT', 'dB0h55Vvp9', 'Ae90rPTimE', NULL, NULL),
(14, 'CkvvDEnMr0', 'flxV3Bb6H6@gmail.com', '$2y$10$dOvvCS3.sPN2CwUN6eRVoOIvaBTlhcH9cFIrbJKdb95zR1nTgptkG', 'nPWjgHUb8e', 'yFLHkC5DUn', 'OqzXQcLOMe', 'KnN6ThU0eP', 'PXQnotH0nW', NULL, NULL),
(15, 'zLynjtNMCP', '1Y64cJa4ar@gmail.com', '$2y$10$iAaE0fzJ7k5YQd.2KKMQ2O5YyWsusAYipDTFjICqruwlU/N0FfL7O', 'wApb7kBohF', 'spQk4syybY', 'ftPK3y8PkC', 'Z4Xnannhf0', 'bR0lSOvnHR', NULL, NULL),
(16, '2HXRsrbKzR', 'fdEr2vuOhr@gmail.com', '$2y$10$qfjVYyE7fAHtczqNFyS53u68tQ7nYi8PwxZSJSIQhQGMnY6sPYMLC', 'WWFytRASXQ', 'gfjTyd7LZz', 'uBakgt0uMx', 'm0lg6D4YM7', 'buN3JZlySD', NULL, NULL),
(17, 'C94f1m1x83', 'cOUXlFQ1le@gmail.com', '$2y$10$01acPZuAB9qk2p5O7q41U.xpwY..WedUk7h7.e2/aDF8FmwhrA2kq', '1bL0fFGaxL', 'S00SQzXar1', 'KGcNHGNpf0', '7RaeNIHHhp', 'X8RJU9xRlf', NULL, NULL),
(18, 'wT6i667QEb', 'o3cMpEykKA@gmail.com', '$2y$10$woXHfJwFBvj.aDnqH0kHS.g2GV1b4snsG4CIMSkNgRsPhihUY.pe6', 'VLKzeHFH1Z', 'jJd5foE5BA', '5YQXfqY9nw', 'gZz4SI9t4F', 'bYjoOQPP5g', NULL, NULL),
(19, 'ZMucWKmd44', 'H8ywsGAC0h@gmail.com', '$2y$10$Dl4ku0BLjoViP/9L0Vs4ceWHebhb6fOySik7VVKc5Dt8gzV65M9aO', '1NhD81bmyW', 'YemHbxmIOz', 'pWtQJxjh1m', 'eqhFDIwemn', '6kVaeirOia', NULL, NULL),
(20, 'd', 'd@gmail.com', '$2y$10$sXANXUHBW6Kndow8lK9eve8B0almSlKWsGK6RqJJHkNvEPqUDfFhy', NULL, NULL, NULL, NULL, 'uuR9vGJpAIiHLzS61tDvDdj8AxKK8a3eZE85zE1LxkHGQr60BWAGLbQoiZjN', '2019-11-03 04:03:51', '2019-11-03 04:03:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
