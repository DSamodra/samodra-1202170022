<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/detail', 'DetailPostController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile','ProfileController@index');
Route::get('/addpost', 'AddpostController@index');
Route::get('/add', 'AddpostController@create');
Route::get('/edit','EditController@index');
Route::get('/like','DetailPostController@store');
Route::get('/comment','AddpostController@store');




