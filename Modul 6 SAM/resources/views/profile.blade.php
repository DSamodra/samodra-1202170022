@extends('layouts.app')

@section('content')
   
    <div class="container">
       <div class="pp">
            <img src="{{ Auth::user()->avatar }}" alt="gamabar avatar" width="150px">
       </div>
       <div class="informasi">
            <div class="add"> 
                <a href="{{url('/addpost')}}">add new post</a>
            </div>
            <div class="jeneng">
                <h4>{{ Auth::user()->name }}</h4>
            </div>
            <div>
                <a href="{{url('/edit')}}">Edit Profile</a> 
                <p>2 Post</p>          
            </div>
            <div>
                <b>{{ Auth::user()->title }}</b>
                <p>{{ Auth::user()->description }}</p>
                <a href="{{ Auth::user()->url }}"> {{ Auth::user()->url }}</a>
            </div>
       </div>
    </div>
    @foreach ($data as $r)
    <div class="images">
            <img src="{{$r->image}}" alt="gamabar post" width="450px" >
    </div>
    @endforeach
    
@endsection
