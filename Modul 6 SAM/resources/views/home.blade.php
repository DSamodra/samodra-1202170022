@extends('layouts.app')

@section('content')
    @foreach ($data as $r)
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="img">
                            <img class="logo" src="{{ Auth::user()->avatar }}" alt="" width="3%" height="auto">
                        </div>
                        <div class="name">
                        {{ Auth::user()->name }}
                        </div>
                        
                    </div>
                    
                    <!--check connection and image-->
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{url('/detail')}}"> <img src="{{$r->image}}" alt="logo" width="100%" height="100%" style="margin-left:auto;margin-right:auto" > </a>
                    </div>

                    <div>   
                        <hr>
                        <a href="{{url('/like')}}"><img class="com" src="like.png" alt="like" width="40px"></a>
                        <a href=""> <img class="com" src="comment.png" alt="comment" width="50px"></a>
                        <p class="email"><b>{{ Auth::user()->email }}</b> &nbsp {{$r->caption}}</p>
                        
                        <div class="koment">
                            <form action="{{url('/comment')}}" method="Post">
                                <input type="text" name="comen" id="comen" placeholder="Recipient's username" size="85px">
                                <input type="button" value="Post">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection
