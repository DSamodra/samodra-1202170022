<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .logo{
            display:block;
            float:left;
            width:4%; 
            height:auto;
            margin-right: 2%;
        }
        .line{
            border-left: 2px solid grey;
            height: 100%;
            display:block;
            float:left;
            margin-right:2%;
        }
        .caption , .email{
            padding-left:2%;
        }
        .gambar{
            display:block;
            float:left;
            margin-top:20px;
        }
        .images{
            display:block;
            float:left;
            margin-top:30px;
            margin-left:50px;
        }
        .des{
            display:block;
            float:right;
            padding-right:15%;
        }
        .mid{
            margin:10px 15% 10px 15 %;
        }
        .avatar{
            display:block;
            float:left;
            width:50px; 
            height:auto;
            margin-right: 15px;
        }
        .name{
            display:block;
            float:left;
        }
        .panel-heading{
            padding-bottom:5%;
        }
        .nama{
            margin-top:10%;
            padding-left:20px;
        }
        .pp{
           display:block;
           float:left; 
           margin-right:40px;
           padding-top:20px;
        }
        .informasi{
            display:block;
            margin-left:20px;
        }
        .add{
            display:block;
            float:right;
        }
        .com{
            margin-left: 20px;
            margin-bottom:2%;
        }
        .koment{
            padding-left:2%;
            padding-bottom:2%;
        }
        .ty{
            margin-top:25px;
        }
        .uy{
            margin-left:2%;
            margin-bottom:5%;
        }

    </style>
</head>
<body>
<div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img class="logo"src="logo.jpg" alt="Logo EAD" >
                        <div class ="line"></div>
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                        <a href="{{url('/profile')}}">Profile</a>
                                        <a href="{{url('/home')}}">Home</a>
                                    </li>
                                </ul>
                               
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
