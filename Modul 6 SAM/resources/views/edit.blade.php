@extends('layouts.app')

@section('content')
   <div class="container">
      <form action="Route('edit', $user)" method="POST">
         <h1>Edit Profile</h1>
         
         <p><b>Title</b></p>
         <input type="text" name="text" id="text" placeholder="{{ Auth::user()->title }}" size="90px">
         
      
         <p><b>Description</b></p>
         <input type="text" name="des" id="des" placeholder="{{ Auth::user()->description }}" size="90px">
         
      
         <p><b>URL</b></p>
         <input type="text" name="url" id="url" placeholder="{{ Auth::user()->url }}" size="90px">
         
      
         <p><b>Profile Image</b></p>
         <input type="button" value="Choose File">
            <br>
         <input class ="save" type="submit" value="Save Profile">
      </form>
   </div>
@endsection
