<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 7vh;
            }

            .flex-center {
                display: flex;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10%;
                top: 70%;
            }

            .content {
                text-align: center;
                position: absolute;
                top:30%;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                display:block;
                float:right;
            }
            p {
                display:block;
                float:left;
                margin-left:15%;
                font-weight:bold;
            }
            h1{
                margin-top:5%;
                font-size:350%;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/register') }}">Register</a>
                        <a href="{{ url('/login') }}">Login</a>
                    @endif
                </div>
            @endif        
        </div>
        <div class="content">
            <img src="ead.png" alt="logo png" width="40%" height="auto">
            <h1>Enterprise Application Development</h1>
        </div>
    </body>
</html>
