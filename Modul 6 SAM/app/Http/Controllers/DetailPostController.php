<?php

namespace App\Http\Controllers;
use DB;
use App\detail_post;
use Illuminate\Http\Request;

class DetailPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')
        ->join('posts','users.id','=','posts.user_id')
        ->join('komentar_posts','users.id','=','komentar_posts.user_id')
        ->get();

        return view('detail_post', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new post();
        $post->like = $request->like;
        $post->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detail_post  $detail_post
     * @return \Illuminate\Http\Response
     */
    public function show(detail_post $detail_post)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detail_post  $detail_post
     * @return \Illuminate\Http\Response
     */
    public function edit(detail_post $detail_post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detail_post  $detail_post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detail_post $detail_post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detail_post  $detail_post
     * @return \Illuminate\Http\Response
     */
    public function destroy(detail_post $detail_post)
    {
        //
    }
}
