<?php

namespace App\Http\Controllers;

use App\addpost;
use Illuminate\Http\Request;

class AddpostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('addpost');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $post = new Post();
        $post->caption = $request -> caption;
        $post ->save();

        return view('home');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new post();
        $post->comment = $request->comment;
        $post->user_id = $request->user_id;
        $post->post_id = $request->post_id;
        $post->save();
        return view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\addpost  $addpost
     * @return \Illuminate\Http\Response
     */
    public function show(addpost $addpost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\addpost  $addpost
     * @return \Illuminate\Http\Response
     */
    public function edit(addpost $addpost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\addpost  $addpost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, addpost $addpost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\addpost  $addpost
     * @return \Illuminate\Http\Response
     */
    public function destroy(addpost $addpost)
    {
        //
    }
}
