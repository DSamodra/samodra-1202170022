<?php  
    SESSION_START();  
    require 'conn.php';  
    $unamed = $_SESSION["uname"];
    $emaild = $_SESSION["em"];
    $id = $_SESSION["user"];

    //query
    $data = mysqli_query($conn, "SELECT * FROM cart_table WHERE user_id='$id'");
    $x=0;
    $harga=0;

    foreach ($data as $a):{
        $harga=$harga + $a['price'];
    }
    endforeach;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
      
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <style>
            .navbar{
                margin-bottom:10px;
                padding-top:20px;
            }
            .login , .reg{
                float:right;
                margin-top:15px;
                margin-bottom:15px;
                color: darkgrey;
                text-decoration:none;
            }
            .login{
                margin-left:75%;
                margin-right:50px;
            }
            .reg{
                margin-right:5%;
            }
            .isi{
                background-color: white;
                padding-top: 20px;
                padding-bottom: 20px;
                padding-left: 20px;
                padding-right: 20px;
                margin-left: 200px;
                margin-right: 200px;
                margin-top: 20px;
                margin-bottom: 2px;
            }
            .icon{
                width:30px;
                height:30px;
            }
            .konten{
                background-color: white;
                margin-left: 200px;
                margin-right: 200px;
                margin-top: 20px;
                margin-bottom: 2px;
            }
            td, th{
                text-align:center;
                padding-top:10px;
                padding-bottom:10px;
            }
            .deleteico{
                width:30px;
                height:30px;
                margin-left:20px;
            }
            
            </style>
    </head>

    <body>
        <!-- navbar atas-->
        <div class ="navbar">
            <a href="Home.login.php"><img class="logo" src="img/ead.png" alt="logo ead" width="160" height="50"></a>
            <a class="reg" href="cart.php"><img class="icon" src="img/cart.png" alt="chart icon"></a>
            <a class="login" href="profile.php"> <?php echo $unamed; ?></a>
        </div>
        <!--navbar atas close-->
        
        <div class ="konten">
            <hr>
            <div class="isi">
            <table align="center" border="0px;" cellpadding="10" cellspacing="0" width="70%" height="20px">

                <tr>
                    <th>No</th>
                    <th>Product</th>
                    <th>Price</th>
                </tr>

                <?php foreach ($data as $a):{
                    $x=$x+1;}?>

                    <tr>
                        <td><?= $x;?></td>
                        <td><?= $a['product'];?></td>
                        <td><?= $a['price'];?></td>
                        <td><a href="detele.php?id=<?= $a['id'];?>" onclick="return confirm('Apakah anda yakin menghapus data ini ?');"><img class="deleteico" src="img/detele.jpg" alt="logo hapus"></a></td>
                    </tr>

                <?php endforeach; ?>

                <tr>
                   <td colspan="2"> Total Harga</td>  
                   <td><?=$harga ?></td>   
                </tr>

             
            </table>
                    
                
            </div>
        </div>
    </body>
</html>