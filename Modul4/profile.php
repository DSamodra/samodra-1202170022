<?php
    SESSION_START();    
    require 'conn.php';  
    $unamed = $_SESSION["uname"];
    $emaild = $_SESSION["em"];
    $nohp = $_SESSION["hp"];
    $pwd = $_SESSION["pwd"];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        
      
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style media="screen">
            .navbar{
                margin-bottom:10px;
                padding-top:20px;
            }
            .login , .reg{
                float:right;
                margin-top:15px;
                margin-bottom:15px;
                color: darkgrey;
                text-decoration:none;
            }
            .login{
                margin-left:75%;
                margin-right:50px;
            }
            .reg{
                margin-right:5%;
            }
            .icon{
                width:30px;
                height:30px;
            }
            .profiledata{
                margin-top:20px;
                margin-bottom:20px;
            }
            th{
                margin-right:200px;
            }
           .isi{
               text-align:center;
           }
           .n ,.m ,.p, .z{
                margin-left: 20px;
                margin-right: 15px;
                border-radius: 5px;
                padding: 10px 10px 10px 10px;
                margin-top: 10px;
                margin-bottom:10px;
                width: 280px;
                height: 40px;
                background-color: white;
                border:1px solid grey;
           }
           .save , .exit{
            border-radius: 10px;
            width: 240px;
            height: 50px;
           }
           .save{
                background-color:white;
                color:lightgreen;
                border:1px solid lightgreen;
            }
            .save:hover{
                background-color:lightgreen;
                color:white;
            }
            .exit{
                background-color:#f24438;
                color: white;
                border:none;
            }
            .exit:hover{
                background-color: darkorange;
            }
            .a{
                border:none;
            }
            .logout{
                border-radius: 10px;
                width: 150px;
                height: 30px;
                margin-top:40px;
                background-color: darkgrey;
                color:white;
            }
            .logout:hover{
                background-color:lightgrey;
                color:black;
            }
            </style>
    </head>

    <body>
        <!-- navbar atas-->
        <div class ="navbar">
            <a href="Home.login.php"><img class="logo" src="img/ead.png" alt="logo ead" width="160" height="50"></a>
            <a class="reg" href="cart.php"><img class="icon" src="img/cart.png" alt="chart icon"></a>
            <a class="login" href="profile.php"> <?php echo $unamed; ?></a>
        </div>
        <!--navbar atas close-->
        
        <div class ="konten">
            <hr>
            <div class="isi">
                <h2>Profile</h2>
                
                <form action="update.php" method="POST">
                    <fieldset>
                        <table class="profiledata" align="center";>
                            <tr>
                                <th>Email</th>
                                <td>:</td>
                                <td><input class="a" type="email" name="email" id="email" value="<?php echo $emaild;?>" readonly></td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td>:</td>
                                <td><input class="n" type="text" name="nama" id="nama" value="<?php echo $unamed; ?>"></td>
                            </tr>
                            <tr>
                                <th>Mobile Phone</th>
                                <td>:</td>
                                <td><input class="m" type="number" name="hp" id="hp" value="<?php echo $nohp; ?>"></td>
                            </tr>
                            <tr>
                                <th>New Password</th>
                                <td>:</td>
                                <td><input class="p" type="password" name="pwd" id="pwd" placeholder="New Password"></td>
                            </tr>
                            <tr>
                                <th>Confirm Password</th>
                                <td>:</td>
                                <td><input class="z" type="password" name="newpwd" id="newpwd" placeholder="Retype NewPassword"></td>
                            </tr>
                        </table>
                            <input class="exit" type="button" value="Kembali" onclick="history.back()"> 
                            <input class="save" type="submit" value="Save" name="submit"/>
                    </fieldset>
                </form>

                <form action="dc.php" action="POST">
                    <input class="logout" type="submit" value="Logout" name="submit"/>
                </form>
            </div>
        </div>
    </body>
</html>