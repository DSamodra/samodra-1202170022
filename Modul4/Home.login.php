<?php  
    SESSION_START();  
    require 'conn.php';  
    $unamed = $_SESSION["uname"];
    $emaild = $_SESSION["em"];
  
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        
      
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <style>
            .navbar{
                margin-bottom:10px;
                padding-top:20px;
            }
            .login , .reg{
                float:right;
                margin-top:15px;
                margin-bottom:15px;
                color: darkgrey;
                text-decoration:none;
            }
            .login{
                margin-left:75%;
                margin-right:50px;
            }
            .reg{
                margin-right:5%;
            }
            .isi{
                margin-right:24%;
                margin-left:24%;
                margin-top:2%;
            }
            .opening{
                background-color:lightblue;
                margin-bottom:20px;
                padding:80px 20px 80px 20px;
                
            }
            .html , .java , .python{
                float:left;
                background-color: #e6e6e6;
                margin-top:20px;
            }

            .python{
                float:right;
            }

            .java{
                margin-left:25px;
            }

            .imghtml, .imgjav, .imgpy{
                width:250px;
                height:220px;
                margin: 10px 10px 10px 30px;
                padding: 20px 30px 35px 55px;
            }
            .htmlisi , .javaisi , .pythonisi{
                background-color:white;
                margin-left:5px;
                margin-right:5px;
                padding : 10px 10px 10px 10px;
                color:#666769;
            }
            .htmlisi{
                padding-bottom:31px;
            }
            .buy1 , .buy2 , .buy3{
                margin-left: 20px;
                margin-right: 15px;
                border-radius: 5px;
                padding: 10px 10px 10px 10px;
                margin-top: 10px;
                margin-bottom:10px;
                width: 280px;
                height: 40px;
                background-color: #118dfa;
                color: white;
                border:none;
            }
            h5{
                margin-left:45%;
                color:lightgrey;
            }

            #masuk , #register{
                margin-top:170px;
            }
            #uname, #pwd ,#cfmpwd , #email{
                margin-right: 15px;
                border-radius: 5px;
                padding: 10px 10px 10px 10px;
                margin-top: 10px;
                margin-bottom:10px;
                width: 565px;
                height: 40px;
                background-color: white;
                color: black;
                border:1px solid lightgrey;
            }
            .exit , .logon{
                margin-left: 20px;
                margin-right: 15px;
                border-radius: 4px;
                padding: 10px 10px 10px 10px;
                margin-top: 10px;
                margin-bottom:10px;
                width: 100px;
                height: 40px;
            }
            .exit {
                background-color:#f24438;
                color: white;
                border:none;
            }
            .logon{
                background-color:white;
                color:lightgreen;
                border:1px solid lightgreen;
            }
            .icon{
                width:30px;
                height:30px;
            }
            .buy1:hover , .buy2:hover , .buy3:hover{
                background-color: #87b7fa;
                color:white;
            }
            
            </style>
    </head>

    <body>
        <!-- navbar atas-->
        <div class ="navbar">
            <img class="logo" src="img/ead.png" alt="logo ead" width="160" height="50">
            <a class="reg" href="cart.php"><img class="icon" src="img/cart.png" alt="chart icon"></a>
            <a class="login" href="profile.php"> <?php echo $unamed; ?></a>
        </div>
        <!--navbar atas close-->
        
        <div class ="konten">
            <hr>
            <div class="isi">
                <div class="opening">
                    <h1>Hello Coders</h1>
                    <p>Welcome to our store, please take a look for the product you might buy.</p>
                </div>
                <div class="html">
                    <form action="cart.input.html.php" method="POST">
                        <div>
                            <img class="imghtml" src="img/html.png" alt="logo html" >
                        </div>
                        <div class="htmlisi">
                            <p><b>Learning Basic Web Programming</b></p>
                            <p><b>Rp.210.000,-</b><p>
                            <p>Want to be able to make a website? Learn <br>
                            basic components such as HTML, CSS and <br>
                            JavaScript in this class curriculum.
                            </p>
                        </div>
                        <div>
                            <input class="buy1" type="submit" value="Buy">
                        </div>
                    </form>
                </div>
                <div class="java">
                    <form action="cart.input.java.php" method="POST">
                        <div>
                            <img class="imgjav" src="img/java.png" alt="logo java" >
                        </div>
                        <div class="javaisi">
                            <p><b>Starting Programming in Java</b></p>
                            <p><b>Rp.150.000,-</b></p>
                            <p>Learn Java Language for you who want to <br>
                            learn the most popular Object-Oriented <br>
                            Programming (PBO) concepts for <br>
                            developing applications.</p>
                        </div>
                        <div>
                            <input class="buy2" type="submit" value="Buy">
                        </div>
                    </form>
                </div>
                

                <div class="python">
                    <form action="cart.input.python.php" method="POST">
                        <div>
                            <img class="imgpy" src="img/python.png" alt="logo python">
                        </div>
                    <div class="pythonisi">
                            <p><b>Starting Programming in Python</b></p>
                            <p><b>Rp.200.000,-</b></p>
                            <p>Learn Python - fundamental various<br>
                            current industry trends. Data science,<br>
                            Machine Learning. Infrastructure-<br>
                            management.</p>
                        </div>
                        <div>
                            <input class="buy3" type="submit" value="Buy">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>